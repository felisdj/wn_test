//
//  Page.swift
//  wn_test
//
//  Created by Jackthip Pureesatian on 13/12/2562 BE.
//  Copyright © 2562 Jackthip. All rights reserved.
//

import Foundation

struct Page: Codable {
	var current_page, total_pages, total_items: Int
	var photos: [Photo]
}
