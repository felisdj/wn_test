//
//  Photo.swift
//  wn_test
//
//  Created by Jackthip Pureesatian on 13/12/2562 BE.
//  Copyright © 2562 Jackthip. All rights reserved.
//

import Foundation

struct Photo: Codable {
	var id, positive_votes_count: Int
	var name, description: String
	var image_url: [String]
}
