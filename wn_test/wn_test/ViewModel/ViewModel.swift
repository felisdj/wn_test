//
//  ViewModel.swift
//  wn_test
//
//  Created by Jackthip Pureesatian on 13/12/2562 BE.
//  Copyright © 2562 Jackthip. All rights reserved.
//

import Foundation

protocol ViewModelProtocal: NSObjectProtocol {
	func loadData()
	func getPhoto(atIndex index: Int) -> Photo
	func getPhotoCount() -> Int
	func refresh()
}

class ViewModel: NSObject, ViewModelProtocal {
	
	private let api = API()
	
	private var page, maxPage: Int
	private var list: [Photo]
	
	var loading = BehaviorRelay<Bool>(value: false)
	
	override init() {
		page = 1
		maxPage = 10
		list = []
		super.init()
		loadData()
	}
	
	func refresh() {
		page = 1
		loadData()
	}
	func loadData() {
		if page <= maxPage && loading.value == false {
			loading.accept(true)
			api.getPhotoList(onPage: page) { [weak self] (finished, response) in
				if finished {
					if let page = response as? Page, page.current_page == self?.page ?? 1 {
						if page.current_page == 1 {
							self?.list = []
						}
						self?.list.append(contentsOf: page.photos)
						self?.page += 1
						self?.maxPage = page.total_pages
					}
				}
				self?.loading.accept(false)
			}
		}
	}
	
	func getPhoto(atIndex index: Int) -> Photo {
		return list[index]
	}
	func getPhotoCount() -> Int {
		return list.count + (list.count+1)/5
	}
}
