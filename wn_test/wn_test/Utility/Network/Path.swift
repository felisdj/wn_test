//
//  Path.swift
//  wn_test
//
//  Created by JSobtid on 10/12/2562 BE.
//  Copyright © 2562 Jackthip. All rights reserved.
//

import Foundation

enum Path {
	static func photo(page: Int) -> String {
		return "https://api.500px.com/v1/photos?feature=popular&page=\(page)"
	}
}
