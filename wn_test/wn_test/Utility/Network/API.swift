//
//  API.swift
//  wn_test
//
//  Created by JSobtid on 10/12/2562 BE.
//  Copyright © 2562 Jackthip. All rights reserved.
//

import Foundation
import Alamofire

func print(str: String) {
	#if DEBUG
	print(str)
	#endif
}

typealias complete = (_ complete: Bool, _ response: Any?) -> Void

class API: NSObject {
    
	override init() {
		super.init()
	}
	
	fileprivate func jsonLog(_ dictionary: [String:Any]) -> String {
		var jsonString = ""
		do {
			let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
			jsonString = String.init(data: jsonData, encoding: .utf8)!
		} catch let err {
			jsonString = err.localizedDescription
		}
		return jsonString
	}

	// MARK: - REQUEST
	fileprivate func logAPI(_ url: String, _ typeRequest: String, _ parameter: [String : Any]) {
		//log API
		print(str: "[\(typeRequest)]\nURL : \(url)\nParameter : \(jsonLog(parameter))")
	}
	
	fileprivate func postRequest(url: String, parameter: [String:Any] = [:], complete: @escaping complete) {
		UIApplication.shared.isNetworkActivityIndicatorVisible = true
		
		logAPI(url, "POST", parameter)

		Alamofire.request(url,
						  method: .post,
						  parameters: parameter,
						  encoding: JSONEncoding.default)
			.responseJSON { response in
				UIApplication.shared.isNetworkActivityIndicatorVisible = false
				guard response.result.error == nil else {
					// got an error in getting the data, need to handle it
					print(str: "error calling POST on /todos/1")
					print(str: response.result.error?.localizedDescription ?? "error")
					complete(false, ["message":"error"])
					return
				}
				// make sure we got some JSON since that's what we expect
				guard let json = response.result.value as? [String: AnyObject] else {
					print(str: "didn't get todo object as JSON from API")
					print(str: "Error: \(String(describing: response.result.error))")
					complete(false, ["message":"error"])
					return
				}
				guard let res = response.response else {
					complete(false, json)
					return
				}
				complete(200 <= res.statusCode && res.statusCode < 300, json)
				return
		}
	}
	fileprivate func putRequest(url: String, parameter: [String:Any] = [:], complete: @escaping complete) {
		
		logAPI(url, "PUT", parameter)
		
		Alamofire.request(url,
						  method: .put,
						  parameters: parameter,
						  encoding: JSONEncoding.default)
			.responseJSON { response in
				UIApplication.shared.isNetworkActivityIndicatorVisible = false
				guard response.result.error == nil else {
					// got an error in getting the data, need to handle it
					print(str: "error calling PUT on /todos/1")
					print(str: response.result.error?.localizedDescription ?? "error")
					complete(false,["message":response])
					return
				}
				// make sure we got some JSON since that's what we expect
				guard let json = response.result.value as? [String: AnyObject] else {
					print(str: "didn't get todo object as JSON from API")
					print(str: "Error: \(String(describing: response.result.error))")
					complete(false,["message":response])
					return
				}
				guard let res = response.response else {
					complete(false, json)
					return
				}
				complete(200 <= res.statusCode && res.statusCode < 300, json)
				return
		}
	}
	
	fileprivate func getRequest(url: String, complete: @escaping complete) {
		UIApplication.shared.isNetworkActivityIndicatorVisible = true
		
		logAPI(url, "GET", [:])
		
		Alamofire.request(url,
						  method: .get,
						  parameters: nil,
						  encoding: JSONEncoding.default)
			.responseJSON { response in
				UIApplication.shared.isNetworkActivityIndicatorVisible = false
				guard response.result.error == nil else {
					// got an error in getting the data, need to handle it
					print(str: "error calling GET on /todos/1")
					print(str: response.result.error?.localizedDescription ?? "error")
					complete(false,["message":response])
					return
				}
				// make sure we got some JSON since that's what we expect
				guard let json = response.result.value as? [String: AnyObject] else {
					print(str: "didn't get todo object as JSON from API")
					print(str: "Error: \(String(describing: response.result.error))")
					complete(false,["message":response])
					return
				}
				guard let res = response.response else {
					complete(false, json)
					return
				}
				complete(200 <= res.statusCode && res.statusCode < 300, json)
				return
		}
	}
	fileprivate func patchRequest(url: String, parameter: [String:Any] = [:], complete: @escaping complete) {
		UIApplication.shared.isNetworkActivityIndicatorVisible = true
		
		logAPI(url, "PATCH", parameter)
		
		Alamofire.request(url,
						  method: .patch,
						  parameters: parameter,
						  encoding: JSONEncoding.default)
			.responseJSON { response in
			UIApplication.shared.isNetworkActivityIndicatorVisible = false
			guard response.result.error == nil else {
				// got an error in getting the data, need to handle it
				print(str: "error calling PATCH on /todos/1")
				print(str: response.result.error?.localizedDescription ?? "error")
				complete(false, ["message":"error"])
				return
			}
			// make sure we got some JSON since that's what we expect
			guard let json = response.result.value as? [String: AnyObject] else {
				print(str: "didn't get todo object as JSON from API")
				print(str: "Error: \(String(describing: response.result.error))")
				complete(false, ["message":"error"])
				return
			}
			guard let res = response.response else {
				complete(false, json)
				return
			}
			complete(200 <= res.statusCode && res.statusCode < 300, json)
			return

		}
	}
	
	func getPhotoList(onPage page: Int, _ complete: @escaping complete) {
		getRequest(url: Path.photo(page: page)) { (finished, response) in
			if finished {
				let result = response as? [String:Any] ?? [:]
                do {
					let jsonData = try JSONSerialization.data(withJSONObject: result, options: [])
                    let pageResult = try JSONDecoder().decode(Page.self, from: jsonData)
                    complete(true, pageResult)
                } catch {
                    complete(false, response)
                }
            } else {
                complete(false, response)
            }
            return
		}
	}
}
