//
//  UIView+Custom.swift
//  wn_test
//
//  Created by Jackthip Pureesatian on 13/12/2562 BE.
//  Copyright © 2562 Jackthip. All rights reserved.
//

import UIKit

extension UIView {
	
	func round(radius: CGFloat = 4) {
		clipsToBounds = true
		layer.cornerRadius = radius
	}
	
}
