//
//  ViewController.swift
//  wn_test
//
//  Created by JSobtid on 10/12/2562 BE.
//  Copyright © 2562 Jackthip. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	var refreshControl = UIRefreshControl()
	
	var viewModel: ViewModel = ViewModel()
	var disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
		tableView.refreshControl = refreshControl
		
		setViewModel()
	}
	
	@objc func refreshTableView() {
		viewModel.refresh()
	}
	
	private func setViewModel() {
		viewModel.loading.subscribe(onNext: { [weak self] (loading) in
			if !loading {
				self?.refreshControl.endRefreshing()
				self?.tableView.reloadData()
				self?.tableView.flashScrollIndicators()
			}
		}).disposed(by: disposeBag)
	}
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.getPhotoCount()
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "photoList", for: indexPath) as? PhotoListTableViewCell else {
			return PhotoListTableViewCell()
		}
		if (indexPath.item + 1) % 5 == 0 {
			cell.setInsert()
		} else {
			cell.set(photo: viewModel.getPhoto(atIndex: indexPath.item - (indexPath.item/5)))
		}
		return cell
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if indexPath.item == viewModel.getPhotoCount() - 1 {
			viewModel.loadData()
		}
	}
}
