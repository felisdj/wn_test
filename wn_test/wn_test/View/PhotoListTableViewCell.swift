//
//  PhotoListTableViewCell.swift
//  wn_test
//
//  Created by Jackthip Pureesatian on 10/12/2562 BE.
//  Copyright © 2562 Jackthip. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoListTableViewCell: UITableViewCell {

	@IBOutlet weak var foodImageHeightConstaint: NSLayoutConstraint!
	
	@IBOutlet weak var insertImage: UIImageView!
	@IBOutlet weak var profileImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!
	@IBOutlet weak var likeLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
		profileImageView.round()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

	func setInsert() {
		profileImageView.sd_cancelCurrentImageLoad()
		titleLabel.text = nil
		detailLabel.text = nil
		insertImage.alpha = 1
		foodImageHeightConstaint.constant = 0
	}
	func set(photo: Photo) {
		let numberFormat = NumberFormatter()
		numberFormat.numberStyle = .decimal
		profileImageView.sd_cancelCurrentImageLoad()
		profileImageView.sd_setImage(with: URL(string: photo.image_url.first ?? ""), completed: nil)
		titleLabel.text = photo.name
		detailLabel.text = photo.description
		likeLabel.text = numberFormat.string(from: NSNumber(value: photo.positive_votes_count))
		insertImage.alpha = 0
		foodImageHeightConstaint.constant = 80
	}
	
}
